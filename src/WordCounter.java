import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;


public class WordCounter {
	String message;
	HashMap<String,Integer> wordCount = new HashMap<String,Integer>();
	int count;
	String[] s1;
	
	
	public WordCounter(String message){
		this.message = message;
		s1 = message.split(" ");
//		for(int l = 0 ; l<s1.length;l++){
//			wordCount.put(s1[l],1);
		count();
			
		}
	
	
	public void count(){
		for(int l = 0 ; l<s1.length;l++){
			wordCount.put(s1[l],1);
			if(wordCount.containsKey(s1[l])){
				wordCount.put(s1[l],wordCount.get(s1[l])+1);
			}
			else{
				wordCount.put(s1[l],1);
				
			}
		}
		
		
		
		
	}
	
	public int hasWord(String word){
		if(wordCount.containsKey(word)){
			return wordCount.get(word);
		}
		else{
			return 0;
		}
		
	}
		
	
	public static void main(String[] args) throws IOException{
		//System.out.println("Choose your sentence :");
//		BufferedReader str = new BufferedReader(new InputStreamReader(System.in)); 
//		String s1 = str.readLine(); 
		WordCounter counter = new WordCounter("here is the root of the root and the bud of the bud");
		//System.out.println("Choose your word : ");
		//String s2 = str.readLine();
		counter.count();
		System.out.println(counter.hasWord("root"));
		System.out.println(counter.hasWord("leaf"));
		
	}
	
}
