
public class q2 {
	
	public void methX() throws DataException{
		//Check
		throw new DataException();
	}
	
	// ตอบคำถาม เกิด error ที่ตัว DataExceptionเพราะ DataException is undefind ควรแก้ไขโดยสร้างคลาส DataException ขึ้นมาอีกคลาสเพื่อจะได้บอกว่าตรงจุดนี้มี error และหลัง throw ต้องมี new ด้วย
	
	public void methY() throws FormatException{
		System.err.println("Hello");
		throw new FormatException(); //Uncheck
	}
	//ตอบคำถาม เกิด error เนื่องจาก FormatException cannot be resolved to a type และยังไม่ได้สร้างคลาส FormatExceptionขึ้นมา วิธีแก้ไขทำได้โดยสร้างคลาส FormatException กับเปลี่ยน throws เป็น throw
	
	

}
