
public class q3 {
	public static void main(String[] args){
		try{
			q2 c = new q2();
			System.out.print("A");
			c.methX();
			System.out.print("B");
			c.methY();
			System.out.print("C");
			return;
		}catch (FormatException e){
			System.out.print("D");
		}
		catch (DataException e)	{  
			System.out.print("E");
			
		}finally{
			System.out.print("F");
			
		}
		System.out.print("G");
		
	}

}

//2. หากไม่มี Exception โค้ดนี้จะพิมพ์ข้อความใดออกทางหน้าจอ ตอบ A,B,C,F,G
//3. หาก methX() โยน DataException โค้ดนี้จะพิมพ์ข้อความใดออกทางหน้าจอ ตอบ A,D,F,G
//4. หากไม่มีการโยน Exception ใดใน methX() แต่มีการโยน FormatException ใน methY() โค้ดนี้จะพิมพ์ข้อความใดออกทางหน้าจอ ตอบ A,B,E,F,G
